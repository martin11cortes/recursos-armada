import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../_services';
import { GetusersdbService } from '../getusersdb.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userFirebase

  constructor(public user: UserService, public usersDB: GetusersdbService) { }

  @Input() modulo: string;

  ngOnInit() {
    this.usersDB.getUsuario(this.user.uid)
      .valueChanges().subscribe((_user) => {
        this.userFirebase = _user
      })
  }

}
