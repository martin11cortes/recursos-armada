import { Component, OnInit } from '@angular/core';
import { RecursosService } from '../_services/recursos.service';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../_services/user.service';
import { Recurso } from '../_models/recurso';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-creacion-recurso',
  templateUrl: './creacion-recurso.component.html',
  styleUrls: ['./creacion-recurso.component.css']
})
export class CreacionRecursoComponent implements OnInit {
  myForm: FormGroup;
  recursos: any = [];
  recurso: Recurso;
  recursoSeleccionado: Recurso
  idRecurso
  constructor(
    public UserServices: UserService,
    public fb: FormBuilder,
    private route: ActivatedRoute,
    public CreacionRecurso: RecursosService,
    private toastr: ToastrService) {

    this.CreacionRecurso.getRecursos().valueChanges()
      .subscribe((recursos) => {
        this.recursos = recursos;
      });
    this.myForm = this.fb.group({
      name_resources: ['', [Validators.required]],
      tipo: ['', [Validators.required]],
      description: ['', [Validators.required]],
      cantidad: ['', [Validators.required]]
    });
  }


  ngOnInit() {
    this.idRecurso = this.route.snapshot.params['id']
    this.CreacionRecurso.getRecurso(this.idRecurso).valueChanges().subscribe((_recurso: Recurso) => {
      this.llenarDetalle(_recurso)
    })
  }

  onSubmit(form: NgForm) {
    form.value.id = Math.floor(Math.random() * 100);
    this.recurso = form.value;
    this.CreacionRecurso.createRecurso(this.recurso);
    this.toastr.success('Operacion realizada', 'Recurso creado correctamente!');
    if (form.value.id != null) {
      this.myForm.reset();
    }
  }
  llenarDetalle(recurso: Recurso) {
    this.myForm.controls['name_resources'].setValue(recurso.name_resources)
    this.myForm.controls['tipo'].setValue(recurso.tipo)
    this.myForm.controls['description'].setValue(recurso.description)
    this.myForm.controls['cantidad'].setValue(recurso.cantidad)
    this.myForm.disable()
  }
}