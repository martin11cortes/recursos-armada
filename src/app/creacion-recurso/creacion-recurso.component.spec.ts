import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreacionRecursoComponent } from './creacion-recurso.component';

describe('CreacionRecursoComponent', () => {
  let component: CreacionRecursoComponent;
  let fixture: ComponentFixture<CreacionRecursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreacionRecursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreacionRecursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
