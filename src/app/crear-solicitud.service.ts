import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Solicitud } from './_models/solicitud';
@Injectable({
  providedIn: 'root'
})
export class CrearSolicitudService {
  solicitud : Solicitud;
  constructor(public firebase: AngularFireDatabase) {}

   
   public getSolicitudes() {
          return this.firebase.list('/solicitudes/');
   }
  
   
    public getSolicitud(solicitud : Solicitud) {
      return this.firebase.object('/solicitudes/' + solicitud.id);
    }
  
   
    public createSolicitud(solicitud : Solicitud) {
      
      return this.firebase.database.ref('/solicitudes/' + solicitud.id).set(solicitud);

       
    }
  
   
    public editSolicitud(solicitud : Solicitud) {
      return this.firebase.database.ref('/solicitudes/' + solicitud.id).set(solicitud);
    }
  
  
    public deleteSolicitud(solicitud : Solicitud) {
     
      return this.firebase.database.ref('/solicitudes/' + solicitud.id).remove();
    }
}
