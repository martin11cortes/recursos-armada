﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService } from '../_services';
import { User } from 'firebase';
import { AngularFireList } from 'angularfire2/database';

@Component({
    templateUrl: 'register.component.html',
    styleUrls: ['../login/login.component.css']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    usuarios: any

    constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private alertService: AlertService) {
        this.userService.getUsers().valueChanges().subscribe((usuarios) => {
            this.usuarios = usuarios
        })
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            superior:['', [Validators.required]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            this.alertService.error("Debe completar la información")
            return;
        }

        // this.loading = true;
        this.userService.register(this.registerForm.value).then((e) => {
            this.alertService.success('Registration successful', true)
            this.router.navigate(['/login'])
        }).catch((e) => {
            this.alertService.error(e.message)
        })
    }
}
