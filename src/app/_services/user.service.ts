﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UserService {

    uid: string
    redirectUrl: string
    loggedIn: boolean

    constructor(private http: HttpClient, public firebaseAuth: AngularFireAuth, public firebaseDB: AngularFireDatabase) { }

    getUsers() {
        return this.firebaseDB.list('/usuarios/');
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }

    register(user: User) {
        return this.firebaseAuth.auth.createUserWithEmailAndPassword(user.email, user.password).then((response) => {
            this.saveUser(response.user.uid, user)
        })
    }

    saveUser(uid: string, user: User) {
        user.uid = uid
        return this.firebaseDB.database.ref('/usuarios/' + uid).set(user)
    }

    login(username: string, password: string) {
        return this.firebaseAuth.auth.signInWithEmailAndPassword(username, password)
    }

    update(user: User) {
        // return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/` + id);
    }
}