import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Recurso } from '../_models/recurso';
import{UserService} from '../_services/user.service';

@Injectable({
  providedIn: 'root'
})
export class RecursosService {
  recurso: Recurso

  constructor(public firebase: AngularFireDatabase, public usuario: UserService) { }

  /**
   * getRecursos
   */
  public getRecursos() {
    return this.firebase.list('/recursos/');
  }

  /**
   * getLugar
   */
  public getRecurso(id) {
    return this.firebase.object('/recursos/' + id );
  }

  /**
   * createRecurso
   */

  public createRecurso(recurso: Recurso) {
    
      return this.firebase.database.ref('/recursos/' + recurso.id).set(recurso);
    
  }

  /**
   * editRecurso
   */
  public editRecurso(recurso : Recurso) {
    return this.firebase.database.ref( '/recursos/' + recurso.id).set(recurso);
  }

  /**
   * deleteRecurso
   */
  public deleteRecurso(recurso: Recurso) {
    return this.firebase.database.ref('/recursos/' + recurso.id).remove();
  }
  
  public obtenerRecursoAsignado(usuario : UserService){
    return this.firebase.list( '/usuarios/' + this.usuario.uid + '/RecursoAsignado');
  }

}
