﻿import { Component, OnInit } from '@angular/core';

import { UserService } from '../_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit {
  // currentUser: User;
  tituloModulo = 'Home'
  modulos: Array<any> = []

  constructor(private userService: UserService, private router: Router) {
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.router.config.forEach(ruta => {
      this.modulos.push({
        url: ruta.path,
        titulo: ruta.data.title,
        listado: ruta.data.listado
      })
    })
  }
}