import { TestBed } from '@angular/core/testing';

import { CrearSolicitudService } from './crear-solicitud.service';

describe('CrearSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrearSolicitudService = TestBed.get(CrearSolicitudService);
    expect(service).toBeTruthy();
  });
});
