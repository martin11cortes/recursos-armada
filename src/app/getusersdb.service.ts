import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
var data = {new: 'RecursoAsigando'};
@Injectable({
  providedIn: 'root'
})

export class GetusersdbService {

  constructor(public firebase: AngularFireDatabase) { }

  public getUsuarios() {
    return this.firebase.list('/usuarios/');
 }
 
  public getUsuario(id) {
    return this.firebase.object('/usuarios/' + id);
  }
  public editUsuario(user) {
    return this.firebase.database.ref('/usuarios/' + user.id).set(user);
  }

  public recursoUsuario(id , user) {
     return this.firebase.database.ref('/usuarios/' + id ).child('/RecursoAsignado').push(user);

  }

}
