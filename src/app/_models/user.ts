﻿export class User {
    uid: string
    password: string
    firstName: string
    lastName: string
    email: string
    superior: string
}