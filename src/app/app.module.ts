import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatGridListModule,
  MatCardModule,
  MatSelectModule,
  MatOptionModule,
  MatIconModule,
  MatMenuModule,
  MatListModule,
  MatPaginatorModule,
  MatTableDataSource,
  MatTableModule,
  MatColumnDef,
  MatProgressBarModule,
  MatSortModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ToastrModule } from 'ngx-toastr';

import 'hammerjs';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { RecursosComponent } from './recursos/recursos.component';
import { environment } from '../environments/environment';
import { RecursosService } from './recursos.service';
import { routing } from './app.routing';
import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { CreacionRecursoComponent } from './creacion-recurso/creacion-recurso.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { SolicitudRecursosComponent } from './solicitud-recursos/solicitud-recursos.component';
import { AdminSolicitudComponent } from './admin-solicitud/admin-solicitud.component';
//import { AdminSolicitudComponent } from './admin-solicitud/admin-solicitud.component';
import { ListadoRecursosComponent } from './listado-recursos/listado-recursos.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { UploadsListComponent } from './uploads/uploads-list/uploads-list.component';
import { UploadDetailComponent } from './uploads/upload-detail/upload-detail.component';
import { UploadFormComponent } from './uploads/upload-form/upload-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GetusersdbService } from './getusersdb.service';

@NgModule({
  declarations: [
    AppComponent,
    // RecursosComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    CreacionRecursoComponent,
    SolicitudRecursosComponent,
    AdminSolicitudComponent,
    ListadoRecursosComponent,
    NavbarComponent,
    UploadsListComponent,
    UploadDetailComponent,
    UploadFormComponent,
    PageNotFoundComponent
  ],
  imports: [

    BrowserModule,
    HttpModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpClientModule,
    routing,
    MatGridListModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatPaginatorModule,
    MatTableModule,
    ToastrModule.forRoot(),
    MatProgressBarModule,
    MatSortModule
  ],
  providers: [
    RecursosService,
    AuthGuard,
    AngularFireAuth,
    AlertService,
    AuthenticationService,
    UserService,
    GetusersdbService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }