﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { CreacionRecursoComponent } from './creacion-recurso/creacion-recurso.component';
import { SolicitudRecursosComponent } from './solicitud-recursos/solicitud-recursos.component';
import { AdminSolicitudComponent } from './admin-solicitud/admin-solicitud.component';
import { ListadoRecursosComponent } from './listado-recursos/listado-recursos.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
    {
        path: 'home',
        data: {
            title: 'Home',
            listado: false
        },
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        data: {
            title: 'Login',
            listado: false
        },
        component: LoginComponent
    },
    {
        path: 'register',
        data: {
            title: 'Registro',
            listado: false
        },
        component: RegisterComponent
    },
    {
        path: 'detalle-recurso/:id',
        data:{
            title: 'Detalle',
            listado: false
        },
        component: CreacionRecursoComponent
    },
    {
        path: 'creacion-recurso',
        data: {
            title: 'Creación Recurso',
            listado: true
        },
        canActivate: [AuthGuard],
        component: CreacionRecursoComponent
    },
    {
        path: 'solicitud-recurso',
        data: {
            title: 'Solicitud Recurso',
            listado: true
        },
        component: SolicitudRecursosComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin-solicitud',
        data: {
            title: 'Administración Recurso',
            listado: true
        },
        component: AdminSolicitudComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'listado-recursos',
        data: {
            title: 'Listado Recursos',
            listado: true
        },
        canActivate: [AuthGuard],
        component: ListadoRecursosComponent
    },
    {
        path: 'page-not-found',
        component: PageNotFoundComponent,
        data: {
            title: '404',
            listado: false
        }
    },
    // otherwise redirect to login
    {
        path: '**',
        redirectTo: 'page-not-found',
        data: {
            title: '404',
            listado: false
        }
    }
];

export const routing = RouterModule.forRoot(appRoutes);