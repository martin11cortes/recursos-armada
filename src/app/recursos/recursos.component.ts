import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RecursosService } from '../_services/recursos.service';

@Component({
  selector: 'app-recursos',
  templateUrl: './recursos.component.html',
  styleUrls: ['./recursos.component.css']
})
export class RecursosComponent implements OnInit {

  recursos: any = [];
  constructor(public recursosService: RecursosService) {
    this.recursosService.getRecursos().valueChanges()
      .subscribe((recursos) => {
        this.recursos = recursos;
      });
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    form.value.id = Math.floor(Math.random() * 100);
    
    this.recursosService.createRecurso(form.value);
  }

}
