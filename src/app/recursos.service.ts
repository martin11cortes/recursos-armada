import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserService } from './_services';

@Injectable({
  providedIn: 'root'
})
export class RecursosService {

  constructor(public firebase: AngularFireDatabase, public userService: UserService) { }

  /**
   * getRecursos
   */
  public getRecursos() {
    return this.firebase.list('/usuarios/' + this.userService.uid + '/recursos/');
  }

  /**
   * getLugar
   */
  public getRecurso(id) {
    // return this.firebase.object('/usuarios/' + this.userService.uid + '/recursos/' + id);
    return this.firebase.database.ref('/usuarios/' + this.userService.uid + '/recursos/' + id);
  }

  /**
   * createRecurso
   */
  public createRecurso(recurso) {
    return this.firebase.database.ref('/usuarios/' + this.userService.uid + '/recursos/' + recurso.id).set(recurso);
  }

  /**
   * editRecurso
   */
  public editRecurso(recurso) {
    return this.firebase.database.ref('/usuarios/' + this.userService.uid + '/recursos/' + recurso.id).set(recurso);
  }

  /**
   * deleteRecurso
   */
  public deleteRecurso(recurso) {
    return this.firebase.database.ref('/recursos/' + recurso.id).remove();
  }
}
