import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {  CrearSolicitudService } from '../crear-solicitud.service';
import { RecursosService } from '../_services/recursos.service';
import { UserService  } from '../_services/user.service'; //
import { Solicitud } from '../_models/solicitud';
import { GetusersdbService } from '../getusersdb.service'
import { NG_HOST_SYMBOL } from '@angular/core/src/render3/instructions';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { DISABLED } from '@angular/forms/src/model';
var id_recur;
var id_user;
var id_respo;
@Component({
  selector: 'app-solicitud-recursos',
  templateUrl: './solicitud-recursos.component.html',
  styleUrls: ['./solicitud-recursos.component.css']
})

export class SolicitudRecursosComponent implements OnInit {
  myForm: FormGroup;
  
  solicitudes: Solicitud;
  
 
  recursos : any = [];
  users :  any = [];
  constructor(  public fb: FormBuilder, public RecursosServices: RecursosService, public  CrearSolicitudService: CrearSolicitudService,  
    public UserService : UserService,
    public GetuserdbService : GetusersdbService) {
    this.RecursosServices.getRecursos().valueChanges()
    .subscribe((recursos) => {
      this.recursos = recursos;
    });
  
   
   id_user =  this.UserService.uid;

    this.GetuserdbService.getUsuario(id_user).valueChanges().subscribe((usuarios) => {
     this.users = usuarios;
    
     id_respo = this.users.superior;
    
   });
  // id_respo = this.users.source.value.idAprobador ;
  // id_respo =  this.UserService.respo;
      this.myForm = this.fb.group({           
        observa: ['', [Validators.required]],
         cantidad: ['', [Validators.required]]});
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {

     this.solicitudes = form.value;
     this.solicitudes.id = Math.floor(Math.random() * 100);
     this.solicitudes.estado = 'pendiente';
     this.solicitudes.idRecurso = id_recur;
     this.solicitudes.idSolicitante = id_user ;
    this.solicitudes.idAprobador = id_respo ;
 
    this.CrearSolicitudService.createSolicitud(this.solicitudes);
    if(this.solicitudes.id!=null){
 
      this.myForm.reset();
      alert("Registro");
    }
    
  }
  public onSelectionChange(source) {

    id_recur = source.source.value.id;    
  }
  
}

