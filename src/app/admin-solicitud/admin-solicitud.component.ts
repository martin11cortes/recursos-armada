import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { CrearSolicitudService } from '../crear-solicitud.service';
import { GetusersdbService } from '../getusersdb.service'
import { UserService } from '../_services/user.service'; //
import { RecursosService } from '../_services/recursos.service';
import { element } from 'protractor';
import { SolicitudRecursosComponent } from '../solicitud-recursos/solicitud-recursos.component';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { debug } from 'util';
var id_recur;
var id_user;
var razon;
var id_respo;
var recubackup: any = [];
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
@Component({
  selector: 'app-admin-solicitud',
  templateUrl: './admin-solicitud.component.html',
  styleUrls: ['./admin-solicitud.component.css']
})

export class AdminSolicitudComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  displayedColumns2: string[] = ['id', 'idRecurso', 'cantidad', 'idSolicitante', 'observa', 'estado', 'actions'];

  dataSource2;
  solicitudes: any[];
  recurso: any = [];
  users: any = [];
  recursosser: any = [];
  search: any[];
  applyFilter(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }
  constructor(
    public CrearSolicitudService: CrearSolicitudService,
    public GetuserdbService: GetusersdbService,
    public UserService: UserService,
    public RecursosServices: RecursosService) {
    razon = "";
    this.CrearSolicitudService.getSolicitudes().valueChanges()
      .subscribe((solicitudes) => {
        this.dataSource2 = new MatTableDataSource(solicitudes);
        this.solicitudes = solicitudes;
      });

    id_user = this.UserService.uid;

    this.GetuserdbService.getUsuario(id_user).valueChanges().subscribe((usuarios) => {
      this.users = usuarios;
    });
    this.RecursosServices.getRecursos().valueChanges().subscribe((recur) => {
      this.recursosser = recur;
      recubackup = this.recursosser;
    });
  }
  ngOnInit() {
  }
  public eliminar(element) {
    var txt;
    var r = confirm("¿Desea Eliminar?");
    if (r == true) {
      txt = "Eliminado correctamente";
      this.CrearSolicitudService.deleteSolicitud(element);
    } else {
      txt = "Usuario cancelo!";
    }
  }
  public aprobar(element) {
    recubackup.forEach(elements => {
      if (element.idRecurso == elements.id) {
        if (element.cantidad > elements.cantidad) {
          //NO se puede restar
          razon = "falta de recursos ";
          this.rechazar(element);
        } else {
          element.estado = 'Aprobado';
          element.observa = 'Aprobado';
          this.CrearSolicitudService.editSolicitud(element);
          this.recurso.IdRecurso = element.idRecurso;
          this.recurso.cantidad = element.cantidad;
          this.GetuserdbService.recursoUsuario(id_user, this.recurso);
          elements.cantidad = elements.cantidad - element.cantidad;
          this.RecursosServices.editRecurso(elements);
        }
      }
    });
  }

  public rechazar(element) {
    var elementcopy;
    elementcopy = element;
    if (razon == null || razon == "") {
      do {
        razon = window.prompt("Ingrese observación", "");
      } while (razon == "");
    }

    if (razon == null || razon == "") {
      element = elementcopy;
    } else {
      element.estado = 'Negado';
      element.observa = 'Negado por: ' + razon;
      this.CrearSolicitudService.editSolicitud(element);
    }
  }
}
