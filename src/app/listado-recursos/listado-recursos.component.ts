import { Component, OnInit, ViewChild } from '@angular/core';
import { RecursosService } from '../_services/recursos.service';
import { UserService } from '../_services/user.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-listado-recursos',
  templateUrl: './listado-recursos.component.html',
  styleUrls: ['./listado-recursos.component.css']
})
export class ListadoRecursosComponent implements OnInit {

  recursos: any = [];
  id: number
  url: string = ""
  displayedColumns: string[] = ['cantidad', 'description', 'file', 'id', 'name_resources', 'tipo'];
  dataSource = new MatTableDataSource(this.recursos);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public recursosService: RecursosService, public userService: UserService) {
    this.recursosService.obtenerRecursoAsignado(this.userService).valueChanges().subscribe((recursos: any) => {
      recursos.forEach(recurso => {
        this.recursosService.getRecurso(recurso.IdRecurso).valueChanges().subscribe(recursoDetallado => {
          this.recursos.push(recursoDetallado)
          this.dataSource = new MatTableDataSource(this.recursos)
        })
      })
    })
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onClick(recurso: any) {
    this.id = recurso.id;
    this.url = document.location.href + "?id=" + this.id;
    window.location.href = this.url;
  }
}
